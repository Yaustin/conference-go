import json
import requests
from .keys import PEXEL_API, WEATHER_API


def get_current_weather(city, state):
    params = {
        "q": f"{city},{state},US",
        "limit": 1,
        "appid": WEATHER_API,
    }
    url = "http://api.openweathermap.org/geo/1.0/direct"
    response = requests.get(url, params=params)
    content = json.loads(response.content)
    try:
        latitude = content[0]["lat"]
        longitude = content[0]["lon"]
    except(KeyError, IndexError):
        return None

    params = {
        "lat": latitude,
        "lon": longitude,
        "appid": WEATHER_API,
        "units": "imperial",
    }
    url = "https://api.openweathermap.org/data/2.5/weather/"
    response = requests.get(url, params=params)
    content = json.loads(response.content)
    try:
        return{
            "description": content["weather"][0]["description"],
            "temp": content["main"]["temp"],
        }
    except (KeyError, IndexError):
        return None


def get_photo(city, state):
    url = f"https://api.pexels.com/v1/search?query={city}, {state}"
    headers = {
        "Authorization": PEXEL_API,
    }
    response = requests.get(url, headers=headers)
    data = json.loads(response.content)
    return data["photos"][0]['src']["original"]
